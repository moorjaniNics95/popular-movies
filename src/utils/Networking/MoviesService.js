import request from './Request'
import axios from 'axios'


function getRequest(url) {
    axios.defaults.baseURL = url
    return request({
        url: url,
        method: 'GET'
    })
}

function getImage(url) {
    return fetch(url, {
        method: 'GET'
    });
}
// function postRequest(userToken, url, params) {
//     axios.defaults.headers.common['ca-token'] = userToken
//     axios.defaults.headers.post['Content-Type'] = 'application/json';

//     return request({
//         url: url,
//         method: 'POST',
//         data: params
//     })
// }

const MoviesService = {
    getRequest, getImage
}

export default MoviesService;