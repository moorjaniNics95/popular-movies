import MovieModel from '../models/MovieModel'
import MoviesService from '../utils/Networking/MoviesService'

// defines action types
export const GET_POPULAR_LIST = 'GET_POPULAR_LIST';
export const GET_POPULAR_LIST_FULFILLED = 'GET_POPULAR_LIST_FULFILLED';
export const GET_POPULAR_LIST_REJECTED = 'GET_POPULAR_LIST_REJECTED';

export const GET_IMAGE = 'GET_IMAGE';
export const GET_IMAGE_FULFILLED = 'GET_IMAGE_FULFILLED';
export const GET_IMAGE_REJECTED = 'GET_IMAGE_REJECTED';

export const fetchMovies = (page) => {
    return dispatch => {
        dispatch(getMoviesStarted());
        MoviesService.getRequest('https://api.themoviedb.org/3/movie/popular?api_key=3c16b1fcd6a6de6df4ddd0dcfbbd1d7d&language=en-US&page=' + page).then(
            (response) => {
                console.log('Received response: ' + JSON.stringify(response))
                let localResponse = new MovieModel(response)
                console.log(localResponse.results)
                dispatch(getMoviesFulfilled(localResponse.moviesObject.results));
            }
        )
            .catch((error) => {
                console.log('Received error: ' + error)
                // dispatch(getMoviesRejected(error));
            });
    }
}

export const fetchMovieImage = (url) => {

    return dispatch => {
        dispatch(getMovieImageStarted());
        MoviesService.getImage(url).then(
            (response) => {
                console.log('Received response: ' + JSON.stringify(response))
                let localResponse = response
                console.log(localResponse)
                dispatch(getMovieImageFulfilled(localResponse));
            }
        )
            .catch((error) => {
                console.log('Received error: ' + error)
                // dispatch(getMovieImageRejected(error));
            });
    }
}

const getMoviesStarted = () => (
    {
        type: GET_POPULAR_LIST
    }
);

const getMoviesFulfilled = (data) => (
    {
        type: GET_POPULAR_LIST_FULFILLED,
        payload: { data }
    }
);

const getMoviesRejected = (error) => (
    {
        type: GET_POPULAR_LIST_REJECTED,
        payload: { error }
    }
);

const getMovieImageStarted = () => (
    {
        type: GET_IMAGE
    }
);

const getMovieImageFulfilled = (data) => (
    {
        type: GET_IMAGE_FULFILLED,
        payload: { data }
    }
);

const getMovieImageRejected = (error) => (
    {
        type: GET_IMAGE_REJECTED,
        payload: { error }
    }
);