import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, ActivityIndicator, TouchableOpacity} from 'react-native';
import ListRow from './ListRow'
import { connect } from 'react-redux';
import { fetchMovies } from './actions/moviesAction'

class ListScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state =
      {
        moviesList: [],
        page:1
      }
  }

  render() {
    return (
        <View>
          {this.PopularMovieList()}

        </View>
    );
  }

  componentDidMount() {
    this.props.getMovies(this.state.page)
  }

  componentWillReceiveProps(newProps) {

    if (newProps.moviesList.data && this.state.page == 1) {
      this.setState({moviesList: newProps.moviesList.data})
    }

  }

  PopularMovieList = () => {
    return (
      <FlatList
        data={this.state.moviesList}
        style={styles.flatview}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={this.renderSeparator}
        ListFooterComponent={this.renderFooter}
        //extraData={this.state.isEditModeOn}
        renderItem={({ item }) =>
          <ListRow
            onPressed={() => this.onAlertClicked(item)}
            item={item}
          />
        }
      />
    )
  }

  renderFooter = () => {
    return (
    //Footer View with Load More button
      <View style={styles.footer}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={this.loadMoreData}
          //On Click of button calling loadMoreData function to load more data
          style={styles.loadMoreBtn}>
          <Text style={styles.btnText}>Load More</Text>
          {this.state.loading ? (
            <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
          ) : null}
        </TouchableOpacity>
      </View>
    );
  }

  loadMoreData = async () => {
    await this.setState({page: this.state.page + 1},()=>{
      this.props.getMovies(this.state.page)
    })

    if(this.state.page!=2){
      await this.setState({moviesList: [...this.state.moviesList, ...this.props.moviesList.data]})
    }
  }
  onAlertClicked = (data) => {

  }

  renderSeparator = () => {
    return (
      <View style={styles.separatorview}></View>
    );
  };

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  flatview: {

  },
  sepview: {
    width: '100%',
    backgroundColor: '#E1E4E6',
    height: 2
  },
  separatorview: {
    backgroundColor: '#E1E4E6',
    height: 1,
    width: "100%",
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});


/*
  Bind state and dispatch actions to props
  */
const mapStateToProps = (state) => ({
  loading: state.moviesData.loading,
  moviesList: state.moviesData.moviesList,
  errorMessage: state.moviesData.errorMessage
});

const mapDispatchToProps = dispatch => {
  return {
    getMovies: (page) => { dispatch(fetchMovies(page)); },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListScreen);