import React from 'react';
import { StyleSheet, View } from 'react-native';

import { Provider, connect } from 'react-redux';
import { store } from './store/configureStore';
import ListScreen from './ListScreen';


export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state =
      {
      }
  }

  render() {
    return (
      <Provider store={store}>
        <View>

          <ListScreen></ListScreen>
        </View>
      </Provider>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  flatview: {

  },
  sepview: {
    width: '100%',
    backgroundColor: '#E1E4E6',
    height: 2
  },
  separatorview: {
    backgroundColor: '#E1E4E6',
    height: 1,
    width: "100%",
  }
});