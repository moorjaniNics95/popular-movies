
export class Movie {
    vote_count = 0
    id = 0
    video = false
    vote_average = 0
    title = ""
    popularity = 0
    poster_path = ""
    original_language = 0
    original_title = ""
    genre_ids = ''
    backdrop_path = ""
    adult = false
    overview = ""
    release_date = ""

    constructor(jsonObject) {
       this.vote_count = jsonObject.vote_count
       this.id = jsonObject.id
       this.video = jsonObject.video
       this.vote_average = jsonObject.vote_average
       this.title = jsonObject.title
       this.popularity = jsonObject.popularity
       this.poster_path = jsonObject.poster_path
       this.original_language = jsonObject.original_language
       this.original_title = jsonObject.original_title
       this.genre_ids = jsonObject.genre_ids
       this.backdrop_path = jsonObject.backdrop_path
       this.adult = jsonObject.adult
       this.overview = jsonObject.overview
       this.release_date = jsonObject.release_date
    }

}


export class MovieObject {
    page = 0
    total_results = 0
    total_pages = 0
    results = []

    constructor(jsonObject) {
        this.page = jsonObject.page
        this.total_results = jsonObject.total_results
        this.total_pages = jsonObject.total_pages

        if (jsonObject.results) {
            jsonObject.results.map(item => {
                this.results.push(new Movie(item))
            })
        }
    }
}


export default class MovieModel {
    moviesObject = ''

    constructor(jsonObject) {
        this.moviesObject = new MovieObject(jsonObject)
    }

}