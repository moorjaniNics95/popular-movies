// import all possible action types
import { GET_POPULAR_LIST, GET_POPULAR_LIST_FULFILLED, GET_POPULAR_LIST_REJECTED, GET_IMAGE, GET_IMAGE_FULFILLED, GET_IMAGE_REJECTED} from '../actions/moviesAction'

// define initial state
const INITIAL_STATE = {
  moviesList: [],
  movieImage:'',
  loading: true,
  errorMessage: null,
  success:false
};

const alertReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case GET_POPULAR_LIST:
      {
          return {...state, loading:true } 
      }
      case GET_POPULAR_LIST_FULFILLED:
      {
          return {...state, loading:false, moviesList:action.payload}         
      }
      case GET_POPULAR_LIST_REJECTED:
      {
          return {...state, loading:false, errorMessage:action.payload}         
      }
      case GET_IMAGE:
      {
          return {...state, loading:true } 
      }
      case GET_IMAGE_FULFILLED:
      {
          return {...state, loading:false, movieImage:action.payload}         
      }
      case GET_IMAGE_REJECTED:
      {
          return {...state, loading:false, errorMessage:action.payload}         
      }
      default:
        return state
    }
  };
  
  
  export default alertReducer;