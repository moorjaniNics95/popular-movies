import { combineReducers } from 'redux';
import MoviesReducer from './moviesReducer'

// all other reducers should be combined in this root reducer
export default combineReducers({
    moviesData: MoviesReducer,
    
})
