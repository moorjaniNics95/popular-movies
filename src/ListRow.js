import React from 'react';
import { ScrollView, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { fetchMovieImage } from './actions/moviesAction'
import sampleImage from '../assets/download.png'

class ListRow extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      movieImage: sampleImage
    }

  }

  componentDidMount() {
    this.props.getImage("http://image.tmdb.org/t/p/w185/"+this.props.item.poster_path)
  }

  componentWillReceiveProps(newProps) {

    if (newProps.movieImage.data) {
      this.setState({
        movieImage: newProps.movieImage.data._bodyBlob
      })
    }

  }

  render() {
    var date = new Date().toDateString()
    let imageUri = "data:image/png;base64," + this.state.movieImage;
    return (
      <TouchableOpacity onPress={this.props.onPressed}>
        <View style={styles.rowStyle}>
          
          <Image style={{width:"30%", padding:20}} source={{uri: imageUri}}></Image>
          <View style={{width:'70%'}}>
            <Text numberofLines={1}style={styles.movieTiltleText}>{this.props.item.title}</Text>
            <Text>{this.props.item.overview}</Text>
          </View>

        </View>
      </TouchableOpacity>
    )

  }
}

const styles = StyleSheet.create({
  leftRowView: {
    marginLeft: 10,
    width: '70%'
  },
  rightRowView: {
    marginRight: 15,
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'space-around'
  },
  rowStyle: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between'
  },
  headingview: {
    color: 'blue',
    marginLeft: 8,
    marginTop: 18,
    fontSize: 18,
    justifyContent: 'center',
    height: 23,

  },
  topicview: {
    marginBottom: 10,
    fontSize: 14,
    marginLeft: 18,
    height: 18,
    color: 'blue',
    justifyContent: 'center'
  },
  typeview: {
    marginBottom: 26,
    fontSize: 14,
    marginLeft: 5,
    color: 'blue',
    justifyContent: 'center'
  },
  regionview: {
    marginBottom: 5,
    marginLeft: 5,
    marginTop: 12,
    fontSize: 14,
    color: 'blue',
    justifyContent: 'center',
    height: 18,
  },
  movieTiltleText: {
    color: 'black',
    fontSize: 20,
    fontWeight: "500",

  },
  roundedButtonStyle: {
    marginTop: 25,
    backgroundColor: 'blue',
    width: 10,
    height: 10,
    borderRadius: 8,

  },
  noButtonStyle: {
    marginTop: 25,
    width: 10,
    height: 10,

  },
  whiteRoundedStyle: {
    marginTop: 26,
    backgroundColor: 'blue',
    width: 18,
    height: 18,
    borderRadius: 16,
    borderColor: '#B7BABD',
    borderWidth: 1
  },
  sepview: {
    height: 1,
    width: '100%',
    backgroundColor: 'blue'
  }

});

/*
  Bind state and dispatch actions to props
  */
 const mapStateToProps = (state) => ({
  loading: state.moviesData.loading,
  movieImage: state.moviesData.movieImage,
  errorMessage: state.moviesData.errorMessage
});

const mapDispatchToProps = dispatch => {
  return {
    getImage: (url) => { dispatch(fetchMovieImage(url)); },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListRow);